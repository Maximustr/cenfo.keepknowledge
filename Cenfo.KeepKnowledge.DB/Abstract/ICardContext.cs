﻿using System.Collections.Generic;
using Cenfo.KeepKnowledge.Common.Abstract;
using Cenfo.KeepKnowledge.Entities;

namespace Cenfo.KeepKnowledge.DB.Abstract
{
    /// <summary>Interface for card context.</summary>
    public interface ICardContext : IBasicService
    {
        /// <summary>Gets cards by user identifier.</summary>
        /// <param name="userId">Identifier for the user.</param>
        /// <returns>The cards by user identifier.</returns>
        List<Card> GetCardsByUserId(int userId);
    }
}