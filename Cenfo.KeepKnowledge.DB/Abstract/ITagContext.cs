﻿using Cenfo.KeepKnowledge.Common.Abstract;

namespace Cenfo.KeepKnowledge.DB.Abstract
{
    /// <summary>Interface for tag context.</summary>
    public interface ITagContext : IBasicService
    {
    }
}