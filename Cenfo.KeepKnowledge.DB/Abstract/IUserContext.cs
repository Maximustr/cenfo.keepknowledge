﻿using Cenfo.KeepKnowledge.Common.Abstract;

namespace Cenfo.KeepKnowledge.DB.Abstract
{
    /// <summary>Interface for user context.</summary>
    public interface IUserContext : IBasicService
    {
    }
}