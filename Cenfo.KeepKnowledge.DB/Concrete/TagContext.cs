﻿using System;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using Cenfo.KeepKnowledge.DB.Abstract;
using Cenfo.KeepKnowledge.DB.KeepKnowledgeDB;
using Cenfo.KeepKnowledge.Ioc.Concrete;
using Cenfo.KeepKnowledge.Ioc.Enum;

namespace Cenfo.KeepKnowledge.DB.Concrete
{
    /// <summary>A tag context.</summary>
    [Dependency(DependencyScope.Transient)]
    public class TagContext : ITagContext
    {
        /// <summary>Context for the keep knowledge database.</summary>
        private readonly KeepKnowledgeDBContext _keepKnowledgeDbContext;

        /// <summary>The mapper.</summary>
        private readonly IMapper _mapper;

        /// <summary>Initializes a new instance of the Cenfo.KeepKnowledge.DB.Concrete.KeepKnowledgeContext class.</summary>
        /// <param name="mapper">The mapper.</param>
        public TagContext(IMapper mapper)
        {
            _mapper = mapper;
            _keepKnowledgeDbContext = new KeepKnowledgeDBContext();
        }

        /// <summary>Creates a new T.</summary>
        /// <typeparam name="T">Generic type parameter.</typeparam>
        /// <param name="entity">The entity.</param>
        /// <returns>A T.</returns>
        public T Create<T>(T entity)
        {
            var tag = _mapper.Map<Tag>(entity);
            _keepKnowledgeDbContext.TAGs.Add(tag);
            _keepKnowledgeDbContext.SaveChanges();
            return _mapper.Map<T>(tag);
        }

        /// <summary>Deletes the given entity.</summary>
        /// <typeparam name="T">Generic type parameter.</typeparam>
        /// <param name="entity">The entity.</param>
        /// <returns>True if it succeeds, false if it fails.</returns>
        public bool Delete<T>(T entity)
        {
            try
            {
                var tag = _mapper.Map<Tag>(entity);
                _keepKnowledgeDbContext.Entry(tag).State = EntityState.Deleted;
                _keepKnowledgeDbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>Gets by identifier.</summary>
        /// <typeparam name="T">Generic type parameter.</typeparam>
        /// <param name="id">The identifier.</param>
        /// <returns>The by identifier.</returns>
        public T GetById<T>(int id)
            => _mapper.Map<T>(_keepKnowledgeDbContext.TAGs.FirstOrDefault(x => x.IdTag == id));

        /// <summary>Updates the given entity.</summary>
        /// <typeparam name="T">Generic type parameter.</typeparam>
        /// <param name="entity">The entity.</param>
        /// <returns>A T.</returns>
        public T Update<T>(T entity)
        {
            var user = _mapper.Map<Tag>(entity);
            _keepKnowledgeDbContext.TAGs.Attach(user);
            _keepKnowledgeDbContext.Entry(user).State = EntityState.Modified;
            _keepKnowledgeDbContext.SaveChanges();
            return _mapper.Map<T>(user);
        }
    }
}