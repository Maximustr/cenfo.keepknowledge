﻿using System;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using Cenfo.KeepKnowledge.DB.Abstract;
using Cenfo.KeepKnowledge.DB.KeepKnowledgeDB;
using Cenfo.KeepKnowledge.Ioc.Concrete;
using Cenfo.KeepKnowledge.Ioc.Enum;

namespace Cenfo.KeepKnowledge.DB.Concrete
{
    /// <summary>A keep knowledge context.</summary>
    [Dependency(DependencyScope.Transient)]
    public class UserContext : IUserContext
    {
        /// <summary>Context for the keep knowledge database.</summary>
        private readonly KeepKnowledgeDBContext _keepKnowledgeDbContext;

        /// <summary>The mapper.</summary>
        private readonly IMapper _mapper;

        /// <summary>Initializes a new instance of the Cenfo.KeepKnowledge.DB.Concrete.KeepKnowledgeContext class.</summary>
        /// <param name="mapper">The mapper.</param>
        public UserContext(IMapper mapper)
        {
            _mapper = mapper;
            _keepKnowledgeDbContext = new KeepKnowledgeDBContext();
        }

        /// <summary>Creates a new T.</summary>
        /// <typeparam name="T">Generic type parameter.</typeparam>
        /// <param name="entity">The entity.</param>
        /// <returns>A T.</returns>
        public T Create<T>(T entity)
        {
            var user = _mapper.Map<UserKeep>(entity);
            _keepKnowledgeDbContext.UserKeeps.Add(user);
            _keepKnowledgeDbContext.SaveChanges();
            return _mapper.Map<T>(user);
        }

        /// <summary>Deletes the given entity.</summary>
        /// <typeparam name="T">Generic type parameter.</typeparam>
        /// <param name="entity">The entity.</param>
        /// <returns>True if it succeeds, false if it fails.</returns>
        public bool Delete<T>(T entity)
        {
            try
            {
                var user = _mapper.Map<UserKeep>(entity);
                _keepKnowledgeDbContext.Entry(user).State = EntityState.Deleted;
                _keepKnowledgeDbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>Gets by identifier.</summary>
        /// <typeparam name="T">Generic type parameter.</typeparam>
        /// <param name="id">The identifier.</param>
        /// <returns>The by identifier.</returns>
        public T GetById<T>(int id)
            => _mapper.Map<T>(_keepKnowledgeDbContext.UserKeeps.FirstOrDefault(x => x.IdUser == id));

        /// <summary>Updates the given entity.</summary>
        /// <typeparam name="T">Generic type parameter.</typeparam>
        /// <param name="entity">The entity.</param>
        /// <returns>A T.</returns>
        public T Update<T>(T entity)
        {
            var user = _mapper.Map<UserKeep>(entity);
            _keepKnowledgeDbContext.UserKeeps.Attach(user);
            _keepKnowledgeDbContext.Entry(user).State = EntityState.Modified;
            _keepKnowledgeDbContext.SaveChanges();
            return _mapper.Map<T>(user);
        }
    }
}