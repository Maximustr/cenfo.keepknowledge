using System;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;

namespace Cenfo.KeepKnowledge.DB.KeepKnowledgeDB
{
    /// <summary>A keep knowledge database context.</summary>
    public partial class KeepKnowledgeDBContext : DbContext
    {
        /// <summary>Initializes a new instance of the Cenfo.KeepKnowledge.DB.KeepKnowledgeDB.KeepKnowledgeDBContext class.</summary>
        public KeepKnowledgeDBContext() : base("name=KeepKnowledge")
        {
            Database.CommandTimeout = 180; //*-*ApiDataProvider.CommandTimeout;

            if (!Debugger.IsAttached) return;

            Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");
            Database.Log = s => Debug.WriteLine(s); //Output SQL to Debug Console

            DateTime initialTime = default(DateTime);

            #region Connection State Changed

            Database.Connection.StateChange += (sender, e) =>
            {
                if (e.CurrentState == ConnectionState.Open)
                    initialTime = DateTime.Now;

                Debug.WriteLine($"{e.OriginalState} => {e.CurrentState}");

                if (e.CurrentState == ConnectionState.Closed)
                    Debug.WriteLine($"DB Command Execution Time: {DateTime.Now - initialTime}");
            };

            #endregion Connection State Changed
        }

        /// <summary>Initializes static members of the Cenfo.KeepKnowledge.DB.KeepKnowledgeDB.KeepKnowledgeDBContext class.</summary>
        static KeepKnowledgeDBContext()
        {
            Database.SetInitializer(new NullDatabaseInitializer<KeepKnowledgeDBContext>()); //Disable DB migrations
        }

        /// <summary>Gets or sets the cards.</summary>
        /// <value>The cards.</value>
        public virtual DbSet<Card> Cards { get; set; }

        /// <summary>Gets or sets the ta gs.</summary>
        /// <value>The ta gs.</value>
        public virtual DbSet<Tag> TAGs { get; set; }

        /// <summary>Gets or sets the user keeps.</summary>
        /// <value>The user keeps.</value>
        public virtual DbSet<UserKeep> UserKeeps { get; set; }

        /// <summary>This method is called when the model for a derived context has been initialized, but
        /// before the model has been locked down and used to initialize the context.  The default
        /// implementation of this method does nothing, but it can be overridden in a derived class
        /// such that the model can be further configured before it is locked down.</summary>
        /// <remarks>Typically, this method is called only once when the first instance of a derived context
        /// is created.  The model for that context is then cached and is for all further instances of
        /// the context in the app domain.  This caching can be disabled by setting the ModelCaching
        /// property on the given ModelBuidler, but note that this can seriously degrade performance.
        /// More control over caching is provided through use of the DbModelBuilder and DbContextFactory
        /// classes directly.</remarks>
        /// <param name="modelBuilder">The builder that defines the model for the context being created.</param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Card>()
                .Property(e => e.Text)
                .IsUnicode(false);

            modelBuilder.Entity<Card>()
                .Property(e => e.Url)
                .IsUnicode(false);

            modelBuilder.Entity<Card>()
                .Property(e => e.TimesToPospone)
                .IsUnicode(false);

            modelBuilder.Entity<Card>()
                .HasMany(e => e.TAGs)
                .WithMany(e => e.Cards)
                .Map(m => m.ToTable("CardTag").MapLeftKey("IdCard").MapRightKey("IdTag"));

            modelBuilder.Entity<Tag>()
                .Property(e => e.Text)
                .IsUnicode(false);

            modelBuilder.Entity<UserKeep>()
                .Property(e => e.FName)
                .IsUnicode(false);

            modelBuilder.Entity<UserKeep>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<UserKeep>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<UserKeep>()
                .Property(e => e.Password)
                .IsUnicode(false);

            modelBuilder.Entity<UserKeep>()
                .HasMany(e => e.Cards)
                .WithRequired(e => e.UserKeep)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserKeep>()
                .HasMany(e => e.TAGs)
                .WithRequired(e => e.UserKeep)
                .WillCascadeOnDelete(false);
        }
    }
}