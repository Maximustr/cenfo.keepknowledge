using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cenfo.KeepKnowledge.DB.KeepKnowledgeDB
{
    /// <summary>A user keep.</summary>
    [Table("UserKeep")]
    public partial class UserKeep
    {
        /// <summary>Initializes a new instance of the Cenfo.KeepKnowledge.DB.KeepKnowledgeDB.UserKeep class.</summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public UserKeep()
        {
            Cards = new HashSet<Card>();
            TAGs = new HashSet<Tag>();
        }

        /// <summary>Gets or sets the identifier user.</summary>
        /// <value>The identifier user.</value>
        [Key]
        public int IdUser { get; set; }

        /// <summary>Gets or sets the name.</summary>
        /// <value>The f name.</value>
        [Required]
        [StringLength(2000)]
        public string FName { get; set; }

        /// <summary>Gets or sets the person's last name.</summary>
        /// <value>The name of the last.</value>
        [Required]
        [StringLength(2000)]
        public string LastName { get; set; }

        /// <summary>Gets or sets the email.</summary>
        /// <value>The email.</value>
        [Required]
        [StringLength(2000)]
        public string Email { get; set; }

        /// <summary>Gets or sets the password.</summary>
        /// <value>The password.</value>
        [Required]
        [StringLength(2000)]
        public string Password { get; set; }

        /// <summary>Gets or sets the cards.</summary>
        /// <value>The cards.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Card> Cards { get; set; }

        /// <summary>Gets or sets the ta gs.</summary>
        /// <value>The ta gs.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tag> TAGs { get; set; }
    }
}