using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cenfo.KeepKnowledge.DB.KeepKnowledgeDB
{
    /// <summary>A card.</summary>
    [Table("Card")]
    public partial class Card
    {
        /// <summary>Initializes a new instance of the Cenfo.KeepKnowledge.DB.KeepKnowledgeDB.Card class.</summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Card()
        {
            TAGs = new HashSet<Tag>();
        }

        /// <summary>Gets or sets the identifier card.</summary>
        /// <value>The identifier card.</value>
        [Key]
        public int IdCard { get; set; }

        /// <summary>Gets or sets the identifier user.</summary>
        /// <value>The identifier user.</value>
        public int IdUser { get; set; }

        /// <summary>Gets or sets the text.</summary>
        /// <value>The text.</value>
        [Required]
        [StringLength(2000)]
        public string Text { get; set; }

        /// <summary>Gets or sets URL of the document.</summary>
        /// <value>The URL.</value>
        [Required]
        [StringLength(2000)]
        public string Url { get; set; }

        /// <summary>Gets or sets the times to pospone.</summary>
        /// <value>The times to pospone.</value>
        [Required]
        [StringLength(2000)]
        public string TimesToPospone { get; set; }

        /// <summary>Gets or sets the Date/Time of the date created.</summary>
        /// <value>The date created.</value>
        public DateTime DateCreated { get; set; }

        /// <summary>Gets or sets the Date/Time of the date updated.</summary>
        /// <value>The date updated.</value>
        public DateTime DateUpdated { get; set; }

        /// <summary>Gets or sets the Date/Time of the next date to modify.</summary>
        /// <value>The next date to modify.</value>
        public DateTime NextDateToModify { get; set; }

        /// <summary>Gets or sets the user keep.</summary>
        /// <value>The user keep.</value>
        public virtual UserKeep UserKeep { get; set; }

        /// <summary>Gets or sets the ta gs.</summary>
        /// <value>The ta gs.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tag> TAGs { get; set; }
    }
}