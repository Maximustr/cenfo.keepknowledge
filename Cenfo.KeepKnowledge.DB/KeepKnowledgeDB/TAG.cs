using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cenfo.KeepKnowledge.DB.KeepKnowledgeDB
{
    /// <summary>A tag.</summary>
    [Table("Tag")]
    public partial class Tag
    {
        /// <summary>Initializes a new instance of the Cenfo.KeepKnowledge.DB.KeepKnowledgeDB.Tag class.</summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Tag()
        {
            Cards = new HashSet<Card>();
        }

        /// <summary>Gets or sets the identifier tag.</summary>
        /// <value>The identifier tag.</value>
        [Key]
        public int IdTag { get; set; }

        /// <summary>Gets or sets the identifier user.</summary>
        /// <value>The identifier user.</value>
        public int IdUser { get; set; }

        /// <summary>Gets or sets the text.</summary>
        /// <value>The text.</value>
        [Required]
        [StringLength(2000)]
        public string Text { get; set; }

        /// <summary>Gets or sets the user keep.</summary>
        /// <value>The user keep.</value>
        public virtual UserKeep UserKeep { get; set; }

        /// <summary>Gets or sets the cards.</summary>
        /// <value>The cards.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Card> Cards { get; set; }
    }
}