﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Cenfo.KeepKnowledge.Ioc.Abstract;
using Cenfo.KeepKnowledge.Ioc.Concrete;
using Newtonsoft.Json.Serialization;
using WebApi.StructureMap;

namespace Cenfo.KeepKnowledge.API
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            var container = StructureMapBootstrapper.Bootstrap("Cenfo.*");
            var iMapper = AutoMapperBoostrapper.Bootstrap("Cenfo.*");
            var dependencyRegister = container.GetInstance<IDependencyRegister>();
            dependencyRegister.Register(iMapper);
            GlobalConfiguration.Configuration.UseStructureMap(container);
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}