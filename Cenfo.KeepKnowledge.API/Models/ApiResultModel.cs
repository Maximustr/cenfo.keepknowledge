﻿using System;
using Newtonsoft.Json;

namespace Cenfo.KeepKnowledge.API.Models
{
    /// <summary>
    ///
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ApiResultModel<T> : IApiBaseModel
    {
        /// <summary>
        /// ApiResultModel
        /// </summary>
        public ApiResultModel()
        {
            Status = "OK";
        }

        /// <summary>
        /// Response Time
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Time { get; set; }

        /// <summary>
        /// Errors found
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Exception Error { get; set; }

        /// <summary>
        /// Error message
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Status Code
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Status { get; set; }

        /// <summary>
        /// Server which sends the response
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Server { get; set; }

        /// <summary>
        /// Result showed
        /// </summary>
        public T Result { get; set; }
    }

    /// <summary>IApiBaseModel</summary>
    public interface IApiBaseModel
    {
        /// <summary>
        /// Response Time
        /// </summary>
        string Time { get; set; }

        /// <summary>
        /// Server response
        /// </summary>
        string Server { get; set; }

        /// <summary>
        /// Error found
        /// </summary>
        Exception Error { get; set; }
    }
}