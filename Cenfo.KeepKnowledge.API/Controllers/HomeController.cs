﻿using System.Web.Mvc;

namespace Cenfo.KeepKnowledge.API.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }
    }
}