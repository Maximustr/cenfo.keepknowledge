﻿using System.Collections.Generic;
using System.Web.Http;
using Cenfo.KeepKnowledge.API.Models;
using Cenfo.KeepKnowledge.Biz.Abstract;
using Cenfo.KeepKnowledge.Entities;

namespace Cenfo.KeepKnowledge.API.Controllers
{
    /// <summary>A controller for handling cards.</summary>
    public class CardController : BaseApiController
    {
        /// <summary>The card service.</summary>
        private readonly ICardService _cardService;

        /// <summary>Initializes a new instance of the Cenfo.KeepKnowledge.API.Controllers.CardController class.</summary>
        /// <param name="cardService">The card service.</param>
        public CardController(ICardService cardService)
        {
            _cardService = cardService;
        }

        /// <summary>Creates a card.</summary>
        /// <param name="card">The card.</param>
        /// <returns>The new card.</returns>
        [HttpPost]
        public ApiResultModel<Card> CreateCard([FromBody]Card card)
            => GetApiResultModel(() => _cardService.Create(card));

        /// <summary>Deletes the card described by card.</summary>
        /// <param name="card">The card.</param>
        /// <returns>An ApiResultModel&lt;bool&gt;</returns>
        [HttpDelete]
        public ApiResultModel<bool> DeleteCard([FromBody]Card card)
            => GetApiResultModel(() => _cardService.Delete(card));

        /// <summary>Gets a card.</summary>
        /// <param name="cardId">Identifier for the user.</param>
        /// <returns>The card.</returns>
        [HttpGet]
        public ApiResultModel<Card> GetCard([FromUri]int cardId)
            => GetApiResultModel(() => _cardService.GetById<Card>(cardId));

        /// <summary>(An Action that handles HTTP GET requests) gets cards by user identifier.</summary>
        /// <param name="userId">Identifier for the user.</param>
        /// <returns>The cards by user identifier.</returns>
        [HttpGet]
        public ApiResultModel<List<Card>> GetCardsByUserId([FromUri]int userId)
            => GetApiResultModel(() => _cardService.GetCardsByUserId(userId));

        /// <summary>Updates the card described by card.</summary>
        /// <param name="card">The card.</param>
        /// <returns>An ApiResultModel&lt;User&gt;</returns>
        [HttpPut]
        public ApiResultModel<Card> UpdateCard([FromBody]Card card)
            => GetApiResultModel(() => _cardService.Update(card));
    }
}