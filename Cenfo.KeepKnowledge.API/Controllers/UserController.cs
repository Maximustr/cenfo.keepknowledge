﻿using System.Web.Http;
using Cenfo.KeepKnowledge.API.Models;
using Cenfo.KeepKnowledge.Biz.Abstract;
using Cenfo.KeepKnowledge.Entities;

namespace Cenfo.KeepKnowledge.API.Controllers
{
    /// <summary>A controller for handling users.</summary>
    public class UserController : BaseApiController
    {
        /// <summary>The user service.</summary>
        private readonly IUserService _userService;

        /// <summary>Initializes a new instance of the Cenfo.KeepKnowledge.API.Controllers.UserController class.</summary>
        /// <param name="userService">The user service.</param>
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>Creates a user.</summary>
        /// <param name="user">The user.</param>
        /// <returns>The new user.</returns>
        [HttpPost]
        public ApiResultModel<User> CreateUser([FromBody]User user)
            => GetApiResultModel(() => _userService.Create(user));

        /// <summary>(An Action that handles HTTP POST requests) deletes the user described by user.</summary>
        /// <param name="user">The user.</param>
        /// <returns>An ApiResultModel&lt;bool&gt;</returns>
        [HttpDelete]
        public ApiResultModel<bool> DeleteUser([FromBody]User user)
            => GetApiResultModel(() => _userService.Delete(user));

        /// <summary>(An Action that handles HTTP GET requests) gets the user.</summary>
        /// <returns>The user.</returns>
        [HttpGet]
        public ApiResultModel<User> GetUser([FromUri]int userId)
            => GetApiResultModel(() => _userService.GetById<User>(userId));

        /// <summary>(An Action that handles HTTP POST requests) updates the user described by user.</summary>
        /// <param name="user">The user.</param>
        /// <returns>An ApiResultModel&lt;User&gt;</returns>
        [HttpPut]
        public ApiResultModel<User> UpdateUser([FromBody]User user)
            => GetApiResultModel(() => _userService.Update(user));
    }
}