﻿using System.Web.Http;
using Cenfo.KeepKnowledge.API.Models;
using Cenfo.KeepKnowledge.Biz.Abstract;
using Cenfo.KeepKnowledge.Entities;

namespace Cenfo.KeepKnowledge.API.Controllers
{
    /// <summary>A controller for handling tags.</summary>
    public class TagController : BaseApiController
    {
        /// <summary>The tag service.</summary>
        private readonly ITagService _tagService;

        /// <summary>Initializes a new instance of the Cenfo.KeepKnowledge.API.Controllers.TagController class.</summary>
        /// <param name="tagService">The tag service.</param>
        public TagController(ITagService tagService)
        {
            _tagService = tagService;
        }

        /// <summary>(An Action that handles HTTP POST requests) creates a tag.</summary>
        /// <param name="tag">The tag.</param>
        /// <returns>The new tag.</returns>
        [HttpPost]
        public ApiResultModel<Tag> CreateTag([FromBody]Tag tag)
            => GetApiResultModel(() => _tagService.Create(tag));

        /// <summary>(An Action that handles HTTP POST requests) deletes the tag described by tag.</summary>
        /// <param name="tag">The tag.</param>
        /// <returns>An ApiResultModel&lt;bool&gt;</returns>
        [HttpDelete]
        public ApiResultModel<bool> DeleteTag([FromBody]Tag tag)
            => GetApiResultModel(() => _tagService.Delete(tag));

        /// <summary>(An Action that handles HTTP GET requests) gets a tag.</summary>
        /// <param name="tagId">Identifier for the tag.</param>
        /// <returns>The tag.</returns>
        [HttpGet]
        public ApiResultModel<Tag> GetTag([FromUri]int tagId)
            => GetApiResultModel(() => _tagService.GetById<Tag>(tagId));

        /// <summary>(An Action that handles HTTP POST requests) updates the tag described by tag.</summary>
        /// <param name="tag">The tag.</param>
        /// <returns>An ApiResultModel&lt;Card&gt;</returns>
        [HttpPut]
        public ApiResultModel<Tag> UpdateTag([FromBody]Tag tag)
            => GetApiResultModel(() => _tagService.Update(tag));
    }
}