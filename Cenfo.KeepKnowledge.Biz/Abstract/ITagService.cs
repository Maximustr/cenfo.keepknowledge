﻿using Cenfo.KeepKnowledge.Common.Abstract;

namespace Cenfo.KeepKnowledge.Biz.Abstract
{
    /// <summary>Interface for tag service.</summary>
    public interface ITagService : IBasicService
    {
    }
}