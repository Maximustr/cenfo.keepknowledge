﻿using Cenfo.KeepKnowledge.Common.Abstract;

namespace Cenfo.KeepKnowledge.Biz.Abstract
{
    /// <summary>Interface for user service.</summary>
    public interface IUserService : IBasicService
    {
    }
}