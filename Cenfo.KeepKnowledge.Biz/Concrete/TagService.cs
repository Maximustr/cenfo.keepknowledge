﻿using Cenfo.KeepKnowledge.Biz.Abstract;
using Cenfo.KeepKnowledge.Common.Concrete;
using Cenfo.KeepKnowledge.DB.Abstract;
using Cenfo.KeepKnowledge.Ioc.Concrete;
using Cenfo.KeepKnowledge.Ioc.Enum;

namespace Cenfo.KeepKnowledge.Biz.Concrete
{
    /// <summary>A service for accessing tags information.</summary>
    [Dependency(DependencyScope.Transient)]
    public class TagService : BasicService, ITagService
    {
        /// <summary>Context for the tag.</summary>
        private readonly ITagContext _tagContext;

        /// <summary>Initializes a new instance of the Cenfo.KeepKnowledge.Biz.Concrete.UserService class.</summary>
        /// <param name="tagContext">Context for the keep knowledge.</param>
        public TagService(ITagContext tagContext) : base(tagContext)
        {
            _tagContext = tagContext;
        }
    }
}