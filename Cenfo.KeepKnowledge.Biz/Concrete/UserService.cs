﻿using Cenfo.KeepKnowledge.Biz.Abstract;
using Cenfo.KeepKnowledge.Common.Concrete;
using Cenfo.KeepKnowledge.DB.Abstract;
using Cenfo.KeepKnowledge.Ioc.Concrete;
using Cenfo.KeepKnowledge.Ioc.Enum;

namespace Cenfo.KeepKnowledge.Biz.Concrete
{
    /// <summary>A service for accessing users information.</summary>
    [Dependency(DependencyScope.Transient)]
    public class UserService : BasicService, IUserService
    {
        /// <summary>Context for the keep knowledge.</summary>
        private readonly IUserContext _userContext;

        /// <summary>Initializes a new instance of the Cenfo.KeepKnowledge.Biz.Concrete.UserService class.</summary>
        /// <param name="userContext">Context for the keep knowledge.</param>
        public UserService(IUserContext userContext) : base(userContext)
        {
            _userContext = userContext;
        }
    }
}