﻿using System.Collections.Generic;
using Cenfo.KeepKnowledge.Biz.Abstract;
using Cenfo.KeepKnowledge.Common.Concrete;
using Cenfo.KeepKnowledge.DB.Abstract;
using Cenfo.KeepKnowledge.Entities;
using Cenfo.KeepKnowledge.Ioc.Concrete;
using Cenfo.KeepKnowledge.Ioc.Enum;

namespace Cenfo.KeepKnowledge.Biz.Concrete
{
    /// <summary>A service for accessing cards information.</summary>
    [Dependency(DependencyScope.Transient)]
    public class CardService : BasicService, ICardService
    {
        /// <summary>Context for the card.</summary>
        private readonly ICardContext _cardContext;

        /// <summary>Initializes a new instance of the Cenfo.KeepKnowledge.Biz.Concrete.UserService class.</summary>
        /// <param name="cardContext">Context for the keep knowledge.</param>
        public CardService(ICardContext cardContext) : base(cardContext)
        {
            _cardContext = cardContext;
        }

        /// <summary>Gets cards by user identifier.</summary>
        /// <param name="userId">Identifier for the user.</param>
        /// <returns>The cards by user identifier.</returns>
        public List<Card> GetCardsByUserId(int userId)
            => _cardContext.GetCardsByUserId(userId);

    }
}