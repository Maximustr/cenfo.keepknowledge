using System.Collections.Generic;

namespace Cenfo.KeepKnowledge.Entities
{
    /// <summary>A tag.</summary>
    public partial class Tag
    {
        /// <summary>Gets or sets the identifier tag.</summary>
        /// <value>The identifier tag.</value>
        public int IdTag { get; set; }

        /// <summary>Gets or sets the identifier user.</summary>
        /// <value>The identifier user.</value>
        public int IdUser { get; set; }

        /// <summary>Gets or sets the text.</summary>
        /// <value>The text.</value>
        public string Text { get; set; }

        /// <summary>Gets or sets the user keep.</summary>
        /// <value>The user keep.</value>
        public virtual User UserKeep { get; set; }

        /// <summary>Gets or sets the cards.</summary>
        /// <value>The cards.</value>
        public virtual ICollection<Card> Cards { get; set; }
    }
}