using System;
using System.Collections.Generic;

namespace Cenfo.KeepKnowledge.Entities
{
    /// <summary>A card.</summary>
    public partial class Card
    {
        /// <summary>Gets or sets the identifier card.</summary>
        /// <value>The identifier card.</value>
        public int IdCard { get; set; }

        /// <summary>Gets or sets the identifier user.</summary>
        /// <value>The identifier user.</value>
        public int IdUser { get; set; }

        /// <summary>Gets or sets the text.</summary>
        /// <value>The text.</value>
        public string Text { get; set; }

        /// <summary>Gets or sets URL of the document.</summary>
        /// <value>The URL.</value>
        public string Url { get; set; }

        /// <summary>Gets or sets the times to pospone.</summary>
        /// <value>The times to pospone.</value>
        public string TimesToPospone { get; set; }

        /// <summary>Gets or sets the Date/Time of the date created.</summary>
        /// <value>The date created.</value>
        public DateTime DateCreated { get; set; }

        /// <summary>Gets or sets the Date/Time of the date updated.</summary>
        /// <value>The date updated.</value>
        public DateTime DateUpdated { get; set; }

        /// <summary>Gets or sets the Date/Time of the next date to modify.</summary>
        /// <value>The next date to modify.</value>
        public DateTime NextDateToModify { get; set; }

        /// <summary>Gets or sets the user keep.</summary>
        /// <value>The user keep.</value>
        public virtual User UserKeep { get; set; }

        /// <summary>Gets or sets the tags.</summary>
        /// <value>The tags.</value>
        public virtual ICollection<Tag> Tags { get; set; }
    }
}