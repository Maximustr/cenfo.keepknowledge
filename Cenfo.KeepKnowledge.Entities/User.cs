﻿using System.Collections.Generic;

namespace Cenfo.KeepKnowledge.Entities
{
    /// <summary>An user.</summary>
    public class User
    {
        /// <summary>Gets or sets the identifier user.</summary>
        /// <value>The identifier user.</value>
        public int IdUser { get; set; }

        /// <summary>Gets or sets the First Name.</summary>
        /// <value>The f First Name.</value>
        public string FName { get; set; }

        /// <summary>Gets or sets the person's last name.</summary>
        /// <value>The name of the last.</value>
        public string LastName { get; set; }

        /// <summary>Gets or sets the email.</summary>
        /// <value>The email.</value>
        public string Email { get; set; }

        /// <summary>Gets or sets the password.</summary>
        /// <value>The password.</value>
        public string Password { get; set; }

        /// <summary>Gets or sets the cards.</summary>
        /// <value>The cards.</value>
        public virtual ICollection<Card> Cards { get; set; }

        /// <summary>Gets or sets the ta gs.</summary>
        /// <value>The ta gs.</value>
        public virtual ICollection<Tag> Tags { get; set; }
    }
}