﻿using Cenfo.KeepKnowledge.Common.Abstract;

namespace Cenfo.KeepKnowledge.Common.Concrete
{
    public class BasicService : IBasicService
    {
        private readonly IBasicService _ctx;

        /// <summary>Initializes a new instance of the Cenfo.KeepKnowledge.Biz.Concrete.UserService class.</summary>
        /// <param name="ctx">Context for the keep knowledge.</param>
        public BasicService(IBasicService ctx)
        {
            _ctx = ctx;
        }

        /// <summary>Creates a new T.</summary>
        /// <typeparam name="T">Generic type parameter.</typeparam>
        /// <param name="entity">The entity.</param>
        /// <returns>A T.</returns>
        public T Create<T>(T entity)
            => _ctx.Create(entity);

        /// <summary>Deletes the given entity.</summary>
        /// <typeparam name="T">Generic type parameter.</typeparam>
        /// <param name="entity">The entity.</param>
        /// <returns>True if it succeeds, false if it fails.</returns>
        public bool Delete<T>(T entity)
            => _ctx.Delete(entity);

        /// <summary>Gets by identifier.</summary>
        /// <typeparam name="T">Generic type parameter.</typeparam>
        /// <param name="id">The identifier.</param>
        /// <returns>The by identifier.</returns>
        public T GetById<T>(int id)
            => _ctx.GetById<T>(id);

        /// <summary>Updates the given entity.</summary>
        /// <typeparam name="T">Generic type parameter.</typeparam>
        /// <param name="entity">The entity.</param>
        /// <returns>A T.</returns>
        public T Update<T>(T entity)
            => _ctx.Update(entity);
    }
}